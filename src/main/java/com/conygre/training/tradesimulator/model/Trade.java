package com.conygre.training.tradesimulator.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade extends Entity {
    private double quantity;
    private String tradetype;
    private TradeState state = TradeState.CREATED;
    private Date tradedate;
    private String ticker;
    private double amount = 0.0;
    

    public double getquantity() {
        return quantity;
    }

    public void setquantity(double quantity) {
        if (quantity<0){
            throw new TradeException("quantity should be > 0");
        }
        else{
        this.quantity = quantity;
        }
    }

    public String getTradetype() {
        return tradetype;
    }

    public void setTradetype(String tradetype) {
        this.tradetype = tradetype;
    }

    public TradeState getstate() {
        return state;
    }

    public void setstate(TradeState state) {
        this.state = state;
    }

    public Date getTradedate() {
        return tradedate;
    }

    public void setTradedate(Date tradedate) {
        this.tradedate = tradedate;
    }

    public String getticker() {
        return ticker;
    }

    public void setticker(String ticker) {
        this.ticker = ticker;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }



    public Trade(String id, Date cdate, double quantity, String tradetype, TradeState state, Date tradedate, String ticker, double amount) {
        super(id, cdate);
        this.quantity = quantity;
        this.tradetype = tradetype;
        this.state = state;
        this.tradedate = tradedate;
        this.ticker = ticker;
        this.amount = amount;
    }

    public Trade() {
        
       
    }

    @Override
    public String toString() {
        return super.toString()+"Trade [state=" + state + ", tradedate=" + tradedate + ", tradetype=" + tradetype + ", ticker="
                + ticker + ", quantity=" + quantity + ", amount="+ amount + "]";
    }
    
}
